import {getPedidos, removePedidos, removePedidos2} from './pedidos'

let pedidos = []

const loadPedidos = function(){
    const pedidosJSON = sessionStorage.getItem('pedidos')
    
    if(pedidosJSON !== null){
        return JSON.parse(pedidosJSON)
    } else {
        return []
    }
}

const cartTable = () => {
    const tabela3 = document.createElement('table')
    document.querySelector('#confirmacao').appendChild(tabela3)

    //header da tabela

    const quantid = document.createElement('th')
    quantid.textContent = 'QTD'
    document.querySelector('table').appendChild(quantid)

    const refere = document.createElement('th')
    refere.textContent = 'ITEM'
    document.querySelector('table').appendChild(refere)

    const unit = document.createElement('th')
    unit.textContent = 'PREÇO'
    document.querySelector('table').appendChild(unit)

    const subt = document.createElement('th')
    subt.textContent = 'SUB-T'
    document.querySelector('table').appendChild(subt)

    const rmv = document.createElement('th')
    rmv.textContent = 'REMOVER'
    document.querySelector('table').appendChild(rmv)
}

const incluiNaDom = (item) => {
    const docElement = document.createElement('tr')
    const tdElement6 = document.createElement('td')
    const tdElement7 = document.createElement('td')
    const tdElement8 = document.createElement('td')
    const tdElement9 = document.createElement('td')
    const tdElement10 = document.createElement('td')
    const button = document.createElement('button')    
    const subTotal = item.qtd * item.preco
        
    tdElement6.textContent = `${item.qtd}`
    docElement.appendChild(tdElement6)

    tdElement7.setAttribute('class', 'item')
    tdElement7.textContent = `${item.produto}`
    docElement.appendChild(tdElement7)

    tdElement8.textContent = `R$${item.preco.toFixed(2).replace('.', ',')}`
    docElement.appendChild(tdElement8)

    tdElement9.setAttribute('class', 'subtotal')
    tdElement9.textContent = `${subTotal}`
    docElement.appendChild(tdElement9)

    docElement.appendChild(tdElement10)
    button.textContent = 'X'

    if(item.tipo !== 'embalagem' && item.tipo !== 'Embalagemhotdog' && item.produto !== 'Emb. Açaí' && item.produto !== 'Emb. à la carte'){
        tdElement10.appendChild(button)
    }
    

    button.addEventListener('click', () => {

        if(item.produto === 'Embalagem' || item.tipo === 'Embalagemhotdog'){
            alert('Não é possível remover')
        }else if(item.produto === 'Combo - Cachorro-Quente' || item.tipo === 'acai' || item.tipo === 'prato') {
            const com = pedidos.indexOf(item)
            removePedidos2(com)
            window.location.reload()
        }
        
        else{
            const a = pedidos.indexOf(item)
            docElement.remove()
            const els = document.querySelectorAll('.subtotal')
            let somando = 0;
            [].forEach.call(els, function(el){
            somando += parseInt(el.textContent);
            })

            const valorTotal = document.createElement('td')
            valorTotal.setAttribute('id', 'total')
            document.querySelector('table').appendChild(valorTotal)
            document.querySelector('#total').textContent = 'R$' + somando.toFixed(2).replace('.', ',')
                  
            removePedidos(a)

            window.location.reload()
        }
        
    })

    if(item.produto === 'Embalagem' && pedidos.filter((x) => x.tipo === 'hamComMolho').length < 1 && item.produto === 'Embalagem' && pedidos.filter((x) => x.tipo === 'linhakids').length < 1 && item.produto === 'Embalagem' && pedidos.filter((x) => x.tipo === 'hamburguerDaPromo').length < 1){
        const b = pedidos.indexOf(item)
        removePedidos(b)
        window.location.reload()
    }

    // if(item.produto === 'Embalagem Hot-Dog' && pedidos.filter((x) => x.tipo === 'comboHotDog').length < pedidos.filter((x) => x.produto === 'Embalagem Hot-Dog').length){
    //     const d = pedidos.indexOf(item)
    //     removePedidos(d)
    //     window.location.reload()
    //     //console.log('exclui embalagem hotdog')
    // }
        
    return docElement
}

const insereCart = () => {
    pedidos.forEach((item) => {
        document.querySelector('table').appendChild(incluiNaDom(item))   
    }) 
}

const rodapeCart = () => {
    const trnew = document.createElement('tr')
    document.querySelector('table').appendChild(trnew)
    

    const tdElTotal = document.createElement('td')
    tdElTotal.setAttribute('colspan', '4')
    tdElTotal.setAttribute('id', 'totalTXT')
    tdElTotal.textContent = "TOTAL"
    trnew.appendChild(tdElTotal)

    const els = document.querySelectorAll('.subtotal')
    let somando = 0;
    [].forEach.call(els, function(el){
        somando += parseInt(el.textContent);
    })

    const valorTotal = document.createElement('td')
    valorTotal.setAttribute('id', 'total')
    trnew.appendChild(valorTotal)
    document.querySelector('#total').textContent = 'R$' + getPedidos().map((e) => e.subt).reduce((a, b) => {
        return a + b
    }, 0).toFixed(2).replace('.', ',')
    
}

pedidos = loadPedidos()

export {cartTable, insereCart, rodapeCart}