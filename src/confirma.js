import {getPedidos, criaPedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'

cartTable()
insereCart()
rodapeCart()

if(getPedidos().map((x) => x.tipo).includes('hamComMolho') && getPedidos().filter((x) => x.tipo === 'embalagem').length < 1 || getPedidos().map((x) => x.tipo).includes('linhakids') && getPedidos().filter((x) => x.tipo === 'embalagem').length < 1 || getPedidos().map((x) => x.tipo).includes('hamburguerDaPromo') && getPedidos().filter((x) => x.tipo === 'embalagem').length < 1){
    criaPedidos(1, 'Embalagem', 1, 'embalagem')
    location.reload()
}

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()

    

    const semTx = getPedidos().filter((x) => x.tipo === 'taxa').length

    console.log(semTx)

    if(semTx < 1){
        location.assign('./txentrega.html')
        console.log('redireciona pra tx')
    }else{

        const valor = getPedidos().filter((x) => x.tipo !== 'taxa').map((e) => e.subt).reduce((a, b) => {
            return a + b
        }, 0)
    
        const retiradaLa = getPedidos().filter((x) => x.tipo === 'taxa')[0].produto === 'Retirada na loja'

        if(getPedidos().map((x)=> x.tipo).includes('taxa') && valor >= 10 || getPedidos().map((x)=> x.tipo).includes('taxa') && retiradaLa){
            location.assign('./forms.html')
            // console.log('forms')
        }if(valor < 10 && !retiradaLa){
            alert('Fazemos entrega para valores acima de R$ 10,00 sem a taxa de entrega. Continue comprando, clique em Comprar Mais')
        }
    }

    

})