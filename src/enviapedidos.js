import {getPedidos} from './pedidos'
import {getClientes} from './cliente'

if(getPedidos().map((x)=> x.tipo).includes('taxa')){
   
}else{
   location.assign('./txentrega.html')
}

const usuarioNome = document.querySelector('#usuario')
usuarioNome.textContent = getClientes()[0]
const valor = getPedidos().map((e) => e.subt).reduce((a, b) => {
    return a + b
}, 0)
const desc = valor * 0.95
const desc2 = valor * 0.95

document.querySelector('.btnFim').addEventListener('click', (e) => {
    e.preventDefault()

    if(getPedidos().filter((x) => x.produto === 'Retirada na loja').length > 0 && getClientes()[6] === 'Dinheiro'){
        location.assign(`https://wa.me/558186008725?text=%2ANome%2A%3A%20${getClientes()[0]}%0A%0A%2ACEP%2A%3A%20${getClientes()[3]}%0A%0A%2AEndere%C3%A7o%2A%3A%20${getClientes()[1]}%2C%20${getClientes()[2]}%0A%0A%2ABairro%2A%3A%20${getPedidos().map((z) => z.nomeDoBairro).join('')}%0A%0A%2ACelular%2A%3A%20${getClientes()[4]}%0A%0A%2APonto%20de%20refer%C3%AAncia%2A%3A%20${getClientes()[5]}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getClientes()[6]}%0A%0A%2ATroco%2A%3A%20${getClientes()[7]}%0A%0A%2AObserva%C3%A7%C3%A3o%2A%3A%20${getClientes()[8]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, x.produto, 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AValor%20Promocional%2A%3A%20R$${desc.toFixed(2).replace('.', ',')}`)
    }else{
        location.assign(`https://wa.me/558186008725?text=%2ANome%2A%3A%20${getClientes()[0]}%0A%0A%2ACEP%2A%3A%20${getClientes()[3]}%0A%0A%2AEndere%C3%A7o%2A%3A%20${getClientes()[1]}%2C%20${getClientes()[2]}%0A%0A%2ABairro%2A%3A%20${getPedidos().map((z) => z.nomeDoBairro).join('')}%0A%0A%2ACelular%2A%3A%20${getClientes()[4]}%0A%0A%2APonto%20de%20refer%C3%AAncia%2A%3A%20${getClientes()[5]}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getClientes()[6]}%0A%0A%2ATroco%2A%3A%20${getClientes()[7]}%0A%0A%2AObserva%C3%A7%C3%A3o%2A%3A%20${getClientes()[8]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, x.produto, 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AValor%20total%2A%3A%20R$${valor.toFixed(2).replace('.', ',')}`)
    }

    // else if(getPedidos().filter((x) => x.produto === 'Retirada na loja').length > 0 && getClientes()[6] !== 'Dinheiro'){        
    //     location.assign(`https://wa.me/558186008725?text=%2ANome%2A%3A%20${getClientes()[0]}%0A%0A%2ACEP%2A%3A%20${getClientes()[3]}%0A%0A%2AEndere%C3%A7o%2A%3A%20${getClientes()[1]}%2C%20${getClientes()[2]}%0A%0A%2ABairro%2A%3A%20${getPedidos().map((z) => z.nomeDoBairro).join('')}%0A%0A%2ACelular%2A%3A%20${getClientes()[4]}%0A%0A%2APonto%20de%20refer%C3%AAncia%2A%3A%20${getClientes()[5]}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getClientes()[6]}%0A%0A%2ATroco%2A%3A%20${getClientes()[7]}%0A%0A%2AObserva%C3%A7%C3%A3o%2A%3A%20${getClientes()[8]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, x.produto, 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AValor%20Promocional%2A%3A%20R$${desc2.toFixed(2).replace('.', ',')}`)
    // }
})



//horario de funcionamento

const dom = 0
const seg = 1
const ter = 2
const qua = 3
const qui = 4
const sex = 5
const sab = 6

let dt = new Date()
let dia = dt.getDay()
let horas = dt.getHours()
let min = dt.getMinutes()

if(dia === sex){
    location.assign('./closed.html')
}else if(dia === seg && horas === 23 && min > 5){
    location.assign('./closed.html')
}else if(dia === ter && horas === 23 && min > 5){
    location.assign('./closed.html')
}else if(dia === qua && horas === 23 && min > 5){
    location.assign('./closed.html')
}else if(dia === qui && horas === 23 && min > 5){
    location.assign('./closed.html')
}

if(dia === dom || dia === seg || dia === ter || dia === qua || dia === qui || dia === sab){
    if(horas < 18){
        alert('O seu pedido será enviado para a Burguerlicious, obrigado pela sua compra! Porém as entregas só começam a partir das 18h. Priorizaremos seu pedido antecipado, assim que abrirmos.') 
    }
}

// else{
//     alert('O seu pedido será enviado para a Burguerlicious, obrigado pela sua compra! Porém as entregas só começam a partir das 18h. Priorizaremos seu pedido antecipado, assim que abrirmos.') 
// }