import {getPedidos} from './pedidos'

let pedidosJuices = []

const loadPedidosJuices = function(){
    const pedidosJuicesJSON = sessionStorage.getItem('pedidosJuices')
    
    if(pedidosJuicesJSON !== null){
        return JSON.parse(pedidosJuicesJSON)
    } else {
        return []
    }
}

const savePedidosJuices = function(){
    sessionStorage.setItem('pedidosJuices', JSON.stringify(pedidosJuices))
}

//expose orders from module
const getPedidosJuices = () => pedidosJuices

const criaPedidosJuices = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosJuices.unshift({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        nomeDoBairro: bairro
    })
    savePedidosJuices()
}

const removePedidosJuices = (item) => {
    pedidosJuices.splice(item, 1)
    savePedidosJuices()
    window.location.reload()
}

const apagaJuices = () => {
    if(getPedidosJuices().filter((x) => x.tipo === 'suco')){
        getPedidosJuices().splice(0, 1)
        savePedidosJuices()        
    }
}

pedidosJuices = loadPedidosJuices()

export { getPedidosJuices, criaPedidosJuices, savePedidosJuices, removePedidosJuices, apagaJuices}