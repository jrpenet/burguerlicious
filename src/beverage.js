import {addMenuBebidas, addMenuBebidasSmoothie} from './menuDrinks'
import {apagaJuices} from './juices'
import {apagaKids} from './refris'

addMenuBebidas()
addMenuBebidasSmoothie()
apagaJuices()
apagaKids()

document.querySelector('.goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})

document.querySelector('.btn-avancaForm').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./txentrega.html')
})