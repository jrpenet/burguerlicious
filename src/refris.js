let pedidosRefris = []

const loadPedidosRefris = function(){
    const pedidosRefrisJSON = sessionStorage.getItem('pedidosRefris')
    
    if(pedidosRefrisJSON !== null){
        return JSON.parse(pedidosRefrisJSON)
    } else {
        return []
    }
}

const savePedidosRefris = function(){
    sessionStorage.setItem('pedidosRefris', JSON.stringify(pedidosRefris))
}

//expose orders from module
const getPedidosRefris = () => pedidosRefris

const criaPedidosRefris = (select, hamb, precoProduto, tx, tipo2) =>{
    
    pedidosRefris.unshift({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        tp2: tipo2
    })
    savePedidosRefris()
}

const removePedidosRefris = (item) => {
    pedidosRefris.splice(item, 1)
    savePedidosRefris()
    window.location.reload()
}

const apagaRefris = () => {
    if(getPedidosRefris().filter((x) => x.tipo === 'freeRefri').length > getPedidosRefris().filter((x) => x.tipo === 'refriAdded').length){
        getPedidosRefris().splice(0, 1)
        savePedidosRefris()        
    }
}

const apagaKids = () => {
    if(getPedidosRefris().filter((x) => x.tipo === 'linhakids')){
        getPedidosRefris().splice(0,1)
        savePedidosRefris()
    }
}

pedidosRefris = loadPedidosRefris()

export { getPedidosRefris, criaPedidosRefris, savePedidosRefris, removePedidosRefris, apagaRefris, apagaKids}