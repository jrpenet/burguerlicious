import {criaPedidosHam} from './hamburguer'

//gera o menu na tela

//cardapio da tela
const cardapioSimple = [{
    nomeHamburguer: 'Tradicional Burguerlicious',
    descricao: 'Hambúrguer de carne bovina 120g, pão tradicional, queijo mussarela, alface, tomate, cebola caramelizada e molho especial.',
    preco: 10,
    tipo: 'hamburguer',
    foto: './images/aaaaa.png'
}, {
    nomeHamburguer: 'Chicken',
    descricao: 'Hambúrguer de filé de frango 120g, com pão tradicional, queijo mussarela, cebola caramelizada, tomate, alface e molho especial.',
    preco: 13,
    tipo: 'hamburguer',
    foto: './images/chicken.jpg'
}, {
    nomeHamburguer: 'Fantastic',
    descricao: 'Hambúrguer de carne bovina fraldinha 120g, pão tradicional, queijo mussarela, cebola caramelizada, tomate, alface e molho especial.',
    preco: 13,
    tipo: 'hamburguer',
    foto: './images/fantastic.jpg'
}, {
    nomeHamburguer: 'Delicious',
    descricao: 'Hambúrguer de carne bovina picanha 120g, pão tradicional, queijo, cebola caramelizada, tomate, alface e molho especial.',
    preco: 16,
    tipo: 'hamburguer',
    foto: './images/delicious.jpg'
}]

const cardapioPremium = [{
    nomeHamburguer: 'Daring',
    descricao: 'Hambúrguer de carne de sol 120g, queijo coalho maçaricado, cebola refogada, tomate e molho especial.',
    preco: 16,
    tipo: 'hamburguer',
    foto: './images/daring.jpg'
}, {
    nomeHamburguer: 'Generous',
    descricao: '2 hambúrgueres de carne bovina 240g, duplo cheddar, bacon, cebola caramelizada e molho especial.',
    preco: 17,
    tipo: 'hamburguer',
    foto: './images/generous.jpg'
}, {
    nomeHamburguer: 'Tasty',
    descricao: 'Hambúrguer artesanal de fraldinha 150g, pão artesanal australiano, queijo provolone maçaricado, calabresa, cebola caramelizada, tomate e molho especial.',
    preco: 18,
    tipo: 'hamburguer',
    foto: './images/tasty.jpg'
}, {
    nomeHamburguer: 'Double Fantastic',
    descricao: '2 hambúrgueres de fraldinha 240g, pão artesanal de provolone, 2 fatias de queijo, cebola refogada, alface, bacon e molho especial.',
    preco: 20,
    tipo: 'hamburguer',
    foto: './images/dblfantastic.jpg'
}, {
    nomeHamburguer: 'Fabulous',
    descricao: 'Hambúguer de picanha 150g, pão artesanal de provolone, queijo do reino maçaricado, bacon, calabresa, cebola caramelizada, tomate e molho especial.',
    preco: 21,
    tipo: 'hamburguer',
    foto: './images/fabulous.jpg'
}, {
    nomeHamburguer: 'All inclusive',
    descricao: 'Bacon, calabresa, ovos, cream cheese, queijo provolone fundido e farofa de bacon',
    preco: 17,
    tipo: 'hamburguer',
    foto: './images/allinclusive.jpeg'
}, {
    nomeHamburguer: 'Tower',
    descricao: '3 carnes de fraldinha, bacon, calabresa, duplo ovo, queijo mussarela, queijo provolone maçaricado, alface, tomate, cebola caramelizada e molho especial.',
    preco: 25,
    tipo: 'hamburguer',
    foto: './images/tower.jpeg'
}]

const cardapioVegan = [{
    nomeHamburguer: 'Soy',
    descricao: 'Hambúrguer de soja 120g com pão tradicional, queijo, alface, tomate, cebola caramelizada e molho especial.',
    preco: 11,
    tipo: 'hamburguer',
    foto: './images/soy.jpg'
}, {
    nomeHamburguer: 'Chickpeas',
    descricao: 'Hambúrguer vegetariano de pão de bico 120g, pão integral de tomate, queijo, alface, tomate, cebola refogada e molho especial.',
    preco: 12,
    tipo: 'hamburguer',
    foto: './images/chickpeas.jpg'
}]

// , {
//     nomeHamburguer: 'Blackbean',
//     descricao: 'Hambúrguer de feijão preto 120g, com pão integral de tomate, alface, tomate, cebola caramelizada e molho vegano.',
//     preco: 11,
//     tipo: 'hamburguer',
//     foto: './images/blackbean.jpg'
// }


const addElementsSimple = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        criaPedidosHam(qt, prodt, price, tp)
        location.assign('./addnsauce.html')
    })

    return divMain
}

const addElementsPremium = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer premium')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        criaPedidosHam(qt, prodt, price, tp)
        location.assign('./addnsauce.html')
    })

    return divMain
}

const addElementsVegan = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer vegan')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        criaPedidosHam(qt, prodt, price, tp)
        location.assign('./addnsauce.html')
    })

    return divMain
}

const addCardapioSimple = () => {
    cardapioSimple.forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElementsSimple(lanche))
        
    })
}

const addCardapioPremium = () => {
    cardapioPremium.forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#mostraPremium').appendChild(hrEl)
        document.querySelector('#mostraPremium').appendChild(addElementsPremium(lanche))
        
    })
}

const addCardapioVegan = () => {
    cardapioVegan.forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#mostraVegan').appendChild(hrEl)
        document.querySelector('#mostraVegan').appendChild(addElementsVegan(lanche))
        
    })
}


export {addCardapioSimple, addCardapioPremium, addCardapioVegan, addElementsVegan, addElementsSimple, addElementsPremium}