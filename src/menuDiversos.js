import {criaPedidos, getPedidos} from './pedidos'
import {criaPedidosHam, getPedidosHam, apagaHams} from './hamburguer'
import {getPedidosJuices} from './juices'
import {getPedidosRefris, apagaKids} from './refris'
import { apagaBolo, getPedidosBolo } from './pedidosBolo'

const cardapioOpcionais = [{
    nomeItem: 'Bacon',
    descricao: '',
    preco: 2,
    tipo: 'opcionalHam'
},{
    nomeItem: 'Cream Cheese',
    descricao: '',
    preco: 2,
    tipo: 'opcionalHam' 
},{
    nomeItem: 'Queijo Coalho',
    descricao: '',
    preco: 2,
    tipo: 'opcionalHam' 
},{
    nomeItem: 'Cheddar',
    descricao: '',
    preco: 2,
    tipo: 'opcionalHam' 
},{
    nomeItem: 'Provolone',
    descricao: '',
    preco: 3,
    tipo: 'opcionalHam' 
},{
    nomeItem: 'Queijo do Reino',
    descricao: '',
    preco: 4,
    tipo: 'opcionalHam' 
},{
    nomeItem: 'Ovos',
    descricao: '',
    preco: 2,
    tipo: 'opcionalHam' 
},{
    nomeItem: 'Calabresa',
    descricao: '',
    preco: 2,
    tipo: 'opcionalHam' 
}]

const cardapioMolhos = [{
    nomeItem: 'Green Sauce (Alho)',
    descricao: '',
    preco: 0,
    tipo: 'molho'
},{
    nomeItem: 'Mayo Temperada',
    descricao: '',
    preco: 0,
    tipo: 'molho'
},{
    nomeItem: 'Spicy (Picante)',
    descricao: '',
    preco: 0,
    tipo: 'molho'
},{
    nomeItem: 'Vegano',
    descricao: '',
    preco: 0,
    tipo: 'molho'
},{
    nomeItem: 'Billy Jack (Bobs)',
    descricao: '',
    preco: 0,
    tipo: 'molho'
}]

const cardapioSabor = [{
    nomeItem: 'Cajá',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Cajú',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Graviola',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Goiaba',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Acerola',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Manga',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Maracujá',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Morango',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Abacaxi',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Abacaxi c/ Hortelã',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Pinha',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
},{
    nomeItem: 'Tamarindo',
    descricao: '',
    preco: 0,
    tipo: 'sabor'
}]

const cardapioRefriCombo = [{
    nomeItem: 'Pepsi',
    descricao: '',
    preco: 0,
    tipo: 'refricombo'
},{
    nomeItem: 'Guaraná',
    descricao: '',
    preco: 0,
    tipo: 'refricombo'
},{
    nomeItem: 'Soda',
    descricao: '',
    preco: 0,
    tipo: 'refricombo'
},{
    nomeItem: 'Sukita',
    descricao: '',
    preco: 0,
    tipo: 'refricombo'
}]

const cardapioSaborBolo = [{
    nomeItem: 'Sensação',
    descricao: '',
    preco: 0,
    tipo: 'saborBolo'
},{
    nomeItem: 'Napolitano',
    descricao: '',
    preco: 0,
    tipo: 'saborBolo'
},{
    nomeItem: 'Bem casado',
    descricao: '',
    preco: 0,
    tipo: 'saborBolo'
},{
    nomeItem: 'Ninho com Nutella',
    descricao: '',
    preco: 0,
    tipo: 'saborBolo'
},{
    nomeItem: 'Prestígio',
    descricao: '',
    preco: 0,
    tipo: 'saborBolo'
}]

const addElementsOpcionais = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeItem
    titulo.setAttribute('class', 'tituloHamburguer acaiopt')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    //divMain.appendChild(descrito)
    //divMain.appendChild(select)
    select.appendChild(opt)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeItem
        const price = lanche.preco
        const type = lanche.tipo

        criaPedidos(qt, prodt, price, type)

        window.location.reload()
        
    })

    return divMain
}

const addElementsMolhos = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeItem
    titulo.setAttribute('class', 'tituloMolho')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    //divMain.appendChild(descrito)
    //divMain.appendChild(select)
    select.appendChild(opt)
    //divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        
        const prodt = lanche.nomeItem
        const trios = getPedidosHam()[0].produto

        if(trios.includes('+')){
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ' (molho ' + prodt + ' incluso)', getPedidosHam()[0].preco, 'hamburguerDaPromo')
            apagaHams()
            location.assign('./confirma.html')
        }
        if(getPedidosHam()[0].tipo === 'hamburguerDaPromo'){
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ' com molho ' + prodt, getPedidosHam()[0].preco, 'hamburguerDaPromo')
            apagaHams()
            location.assign('./confirma.html')
        }else{
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ' com molho ' + prodt, getPedidosHam()[0].preco, 'hamComMolho')
            apagaHams()
            window.location.reload()
        }
        
        
    })

    return divMain
}

const addElementsSabores = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeItem
    titulo.setAttribute('class', 'tituloMolho')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'Escolher'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    //divMain.appendChild(descrito)
    //divMain.appendChild(select)
    select.appendChild(opt)
    //divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeItem
        const price = lanche.preco
        const type = lanche.tipo

        // criaPedidos(qt, prodt, price, type)
        criaPedidos(getPedidosJuices()[0].qtd, getPedidosJuices()[0].produto + ' ' + prodt, getPedidosJuices()[0].preco, 'sucoIn')
        location.assign('./beverage.html')
        
    })

    return divMain
}

const addElementsRefriCombo = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeItem
    titulo.setAttribute('class', 'tituloMolho')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'Escolher'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    //divMain.appendChild(descrito)
    //divMain.appendChild(select)
    select.appendChild(opt)
    //divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeItem
        // const price = lanche.preco
        // const type = lanche.tipo
        // criaPedidos(qt, prodt, price, type)
        if(getPedidosRefris()[0].produto === '2 Trios Fantastic' || getPedidosRefris()[0].produto === '1 Trio Burguerlicious'){
            criaPedidosHam(getPedidosRefris()[0].qtd, getPedidosRefris()[0].produto + ' + (' + prodt + ') 200ml', getPedidosRefris()[0].preco, 'hamburguerDaPromo')
            apagaKids()
            location.assign('./addnsauce.html')
        }else if(getPedidosRefris()[0].produto !== '2 Trios Fantastic' && getPedidosRefris()[0].produto !== '1 Trio Burguerlicious' && getPedidosRefris()[0].tipo === 'linhakids'){
            criaPedidos(getPedidosRefris()[0].qtd, getPedidosRefris()[0].produto + ' + (' + prodt + ') 200ml', getPedidosRefris()[0].preco, 'linhakids')
            apagaKids()
            location.assign('./beverage.html')
        }else{
            criaPedidos(getPedidosRefris()[0].qtd, getPedidosRefris()[0].produto + ' (' + prodt + ') ', getPedidosRefris()[0].preco, 'refriAdded')
            location.assign('./beverage.html')
        }
        
        
        
    })

    return divMain
}

const addElementsSaborBoloCombo = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeItem
    titulo.setAttribute('class', 'tituloMolho')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'Escolher'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    //divMain.appendChild(descrito)
    //divMain.appendChild(select)
    select.appendChild(opt)
    //divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeItem
        const price = lanche.preco
        const type = lanche.tipo
        criaPedidos(getPedidosBolo()[0].qtd, getPedidosBolo()[0].produto + ' (' + prodt + ')', getPedidosBolo()[0].preco, getPedidosBolo()[0].tipo)  
        apagaBolo()
        location.assign('./confirma.html')
    })

    return divMain
}

const addh1Molhos = () => {
    const h1molho = document.createElement('h1')
    h1molho.textContent = 'Molhos Especiais'
    h1molho.setAttribute('class', 'molhosCss')
    document.querySelector('#opcMolhos').appendChild(h1molho)
}

const addh1 = () => {
    const h1 = document.createElement('h1')
    h1.textContent = 'Adicionais'
    h1.setAttribute('class', 'adicionaisCss')
    document.querySelector('#divOpc').appendChild(h1)
}

const addh1Sabores = () => {
    const h1sabor = document.createElement('h1')
    h1sabor.textContent = 'Sabores'
    h1sabor.setAttribute('class', 'molhosCss')
    document.querySelector('#opcSucos').appendChild(h1sabor)
}

const addh1RefriCombos = () => {
    const h1sabor = document.createElement('h1')
    h1sabor.textContent = 'Opções de refri'
    h1sabor.setAttribute('class', 'molhosCss')
    document.querySelector('#refriCombos').appendChild(h1sabor)
}

const addh1PoteCombos = () => {
    const h1sabor = document.createElement('h1')
    h1sabor.textContent = 'Opções de refri'
    h1sabor.setAttribute('class', 'molhosCss')
    document.querySelector('#refriCombos').appendChild(h1sabor)
}

const addCardapioMolhos = () => {
    cardapioMolhos.forEach((lanche) => {
        document.querySelector('#opcMolhos').appendChild(addElementsMolhos(lanche))
    })
}

const addCardapioOpcionais = () => {
    cardapioOpcionais.forEach((lanche) => {
        document.querySelector('#divOpc').appendChild(addElementsOpcionais(lanche))
    })
}

const addCardapioSabores = () => {
    cardapioSabor.forEach((lanche) => {
        document.querySelector('#opcSucos').appendChild(addElementsSabores(lanche))
    })
}

const addCardapioRefriCombos = () => {
    cardapioRefriCombo.forEach((lanche) => {
        document.querySelector('#refriCombos').appendChild(addElementsRefriCombo(lanche))
    })
}

const addCardapioSaborBolo = () => {
    cardapioSaborBolo.forEach((lanche) => {
        document.querySelector('#bolosCombos').appendChild(addElementsSaborBoloCombo(lanche))
    })
}


export {addCardapioOpcionais, addCardapioMolhos, addCardapioSabores, addCardapioRefriCombos, addh1Molhos, addh1Sabores, addh1RefriCombos, addh1, addCardapioSaborBolo}