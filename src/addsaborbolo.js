import {getPedidosBolo} from './pedidosBolo'
import {getPedidos} from './pedidos'
import {addCardapioSaborBolo} from './menuDiversos'

const temBolo = getPedidosBolo().filter((x) => x.produto === 'Bolo de Pote')

if(temBolo.length > 0){
    addCardapioSaborBolo()
}

if(temBolo.length <= 0 || getPedidosBolo() == undefined) {
    location.assign('./sobremesas.html')
}

// document.querySelector('.btn-avanca').addEventListener('click', (e) => {
//     e.preventDefault()

//     const index = getPedidos().length - 1

//     if(getPedidosBolo().filter((x) => x.tipo === 'hamburguer').length > 0){
//         alert('Escolha um molho para acompanhar seu hambúrguer')
//     }
//     if(getPedidos()[index].tipo === 'hamburguerDaPromo' ){
//         location.assign('./confirma.html')
//     }else{
//         location.assign('./combos.html')
//     }
    
// })

