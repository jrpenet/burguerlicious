import { criaPedidos} from './pedidos'

const cardapioFritas = [{
    nomeHamburguer: 'Batatas Fritas Média',
    descricao: '',
    preco: 5,
    tipo: 'batatas',
    foto: ''
},{
    nomeHamburguer: 'Batatas Fritas Grande',
    descricao: '',
    preco: 8,
    tipo: 'batatas',
    foto: ''
},{
    nomeHamburguer: 'Frango Crocante',
    descricao: 'Deliciosas tiras de filé de frango super crocante e levemente apimentadas para alegrar suas noites',
    preco: 10,
    tipo: '',
    foto: './images/chicken.jpg'
},{
    nomeHamburguer: 'Qualquer salgado',
    descricao: '',
    preco: 2,
    tipo: 'batatas',
    foto: ''
},{
    nomeHamburguer: 'Onion Rings (6 unds)',
    descricao: '',
    preco: 4,
    tipo: 'batatas',
    foto: ''
},{
    nomeHamburguer: 'Pão de Alho (2 unds)',
    descricao: '',
    preco: 5,
    tipo: 'batatas',
    foto: ''
},{
    nomeHamburguer: 'Adicional Fritas - Bacon',
    descricao: '',
    preco: 2,
    tipo: 'batatas',
    foto: ''
},{
    nomeHamburguer: 'Adicional Fritas - Cheddar',
    descricao: '',
    preco: 2,
    tipo: 'batatas',
    foto: ''
}]

const addElementsFritas = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    //const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    // imagem.setAttribute('src', lanche.foto)
    // imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    //divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipoEl = lanche.tipo
        criaPedidos(qt, prodt, price, tipoEl)
        location.assign('./beverage.html')
    })

    return divMain
}

const addMenuFritas = () => {
    cardapioFritas.forEach((bebida) => {
        const linhaHoriz = document.createElement('hr')
        document.querySelector('#docFritas').appendChild(linhaHoriz)
        document.querySelector('#docFritas').appendChild(addElementsFritas(bebida))
    })
}

export {addMenuFritas}