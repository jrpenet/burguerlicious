import {criaPedidos, getPedidos} from './pedidos'
import {criaPedidosRefris, apagaRefris} from './refris'
import { apagaHams } from './hamburguer'

apagaHams()
apagaRefris()


//quantidade de hamburguers
const informartn = getPedidos().filter((x) => x.tipo === 'hamComMolho').map((e) => e.qtd).reduce((a, b) => {
    return a + b
}, 0)


const combo = getPedidos().filter((x) => x.tipo === 'combo').length
const comboRefri = getPedidos().filter((x) => x.tipo === 'refriAdded').length
const soma = combo + comboRefri

//console.log(`ham confirmado ${temHam}`)
console.log(`combo1 adicionado ${combo}`)
console.log(`comborefri adicionado ${comboRefri}`)
console.log(informartn)

const combo1 = document.querySelector('#addCombo1')
combo1.addEventListener('click', ()=> {
   if(informartn > combo && informartn > soma){
    criaPedidos(1, 'Combo 1 - Batatas', 4, 'combo')
    alert('Adicionado com sucesso')
    location.reload()
    //location.assign('./beverage.html')
   }else{
       alert('Necessário mais 1 hambúrguer do cardápio principal')
   }
})

const combo2 = document.querySelector('#addCombo2')
combo2.addEventListener('click', ()=> {

    if(informartn > combo && informartn > soma){
        criaPedidosRefris(1, 'Combo 2 - Batatas e refri', 6, 'freeRefri')
        //alert('Adicionado com sucesso')
        //location.reload()
        location.assign('./addjuice.html')
    }else{
        alert('Necessário mais 1 hambúrguer do cardápio principal')
    }

    
})

document.querySelector('.goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})

document.querySelector('.btn-avanca').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./beverage.html')
})