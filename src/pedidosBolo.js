let pedidosBolo = []

const loadPedidosBolo = function(){
    const pedidosBoloJSON = sessionStorage.getItem('pedidosBolo')
    
    if(pedidosBoloJSON !== null){
        return JSON.parse(pedidosBoloJSON)
    } else {
        return []
    }
}

const savePedidosBolo = function(){
    sessionStorage.setItem('pedidosBolo', JSON.stringify(pedidosBolo))
}

//expose orders from module
const getPedidosBolo = () => pedidosBolo

const criaPedidosBolo = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosBolo.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        nomeDoBairro: bairro
    })
    savePedidosBolo()
}

const removePedidosBolo = (item) => {
    pedidosBolo.splice(item, 1)
    savePedidosBolo()
    window.location.reload()
}

const apagaBolo = () => {
    if(getPedidosBolo().filter((x) => x.tipo === 'sobremesa')){
        getPedidosBolo().splice(0,1)
        savePedidosBolo()
    }
}

pedidosBolo = loadPedidosBolo()

export { getPedidosBolo, criaPedidosBolo, savePedidosBolo, removePedidosBolo, apagaBolo}