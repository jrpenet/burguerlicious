import {getPedidosHam} from './hamburguer'
import {criaPedidos} from './pedidos'

    if(getPedidosHam()[0].produto == undefined){
        location.assign('./index.html')
    }

    const molhosEspeciaisCSS = document.querySelectorAll('[name=porcao]')

    molhosEspeciaisCSS.forEach((molho) => {
        molho.addEventListener('change', () => {
            if(molho.checked == true){
                for(let i=0; i < molhosEspeciaisCSS.length; i++){
                    if(molhosEspeciaisCSS[i].checked == true){
                        const pai = molhosEspeciaisCSS[i].parentNode
                        pai.setAttribute('style', 'color: yellow;')
                    }
    
                }
            }
            if(molhosEspeciaisCSS[0].checked == false){
                const pai = molhosEspeciaisCSS[0].parentNode
                pai.setAttribute('style', 'color: red;text-decoration: line-through;')
            }
    
            if(molhosEspeciaisCSS[1].checked == false){
                const pai = molhosEspeciaisCSS[1].parentNode
                pai.setAttribute('style', 'color: red;text-decoration: line-through;')
            }
    
            if(molhosEspeciaisCSS[2].checked == false){
                const pai = molhosEspeciaisCSS[2].parentNode
                pai.setAttribute('style', 'color: red;text-decoration: line-through;')
            }
            if(molhosEspeciaisCSS[3].checked == false){
                const pai = molhosEspeciaisCSS[3].parentNode
                pai.setAttribute('style', 'color: red;text-decoration: line-through;')
            }
            if(molhosEspeciaisCSS[4].checked == false){
                const pai = molhosEspeciaisCSS[4].parentNode
                pai.setAttribute('style', 'color: red;text-decoration: line-through;')
            }
        })
    })
    

    document.querySelector('.btn-avanca').addEventListener('click', (e) => {
        e.preventDefault()

        //molhos
        const molhosEspeciais = document.querySelectorAll('[name=porcao]')
        let meuMolho = ''
        molhosEspeciais.forEach((molho)=>{
            if(molho.checked == true){
                meuMolho = molho.value
            }
        })

        //adicionais
        const adicionais = document.querySelectorAll('.checado')
        let adds = ''
        for(let i =0; i < 6; i++){
            if(adicionais[i].checked == true){
                adds += adicionais[i].value + ', '
            }
        }

    const counterAd = document.querySelectorAll('.checado:checked').length
    //console.log('adicionais' + counterAd)

    //provolone
    const provoloneAd = document.querySelector('.prov')
    //console.log(provoloneAd.checked)

    //Qj do Reino
    const qjReino = document.querySelector('.reino')
    //console.log(qjReino.checked)
    
    //molhos extra
    const molhosExtra = document.querySelectorAll('.molhoChecado')
    let molhoExtra = ''

    for(let i =0; i<5; i++){
        if(molhosExtra[i].checked == true){
            molhoExtra += molhosExtra[i].value + ', '
        }
    }

    const counterMolhoExtra = document.querySelectorAll('.molhoChecado:checked').length
    //console.log(counterMolhoExtra)
    const condicao1 = 'Tradicional Burguerlicious, Generous, Chicken, Fantastic, Delicious, Daring, Tasty, Double Fantastic, Fabulous, All Inclusive, Tower, Soy, Chickpeas'

    if(condicao1.includes(getPedidosHam()[0].produto)){
        if(provoloneAd.checked == true){
            if(qjReino.checked == true){
                criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + provoloneAd.value + ', ' + qjReino.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + 7 + counterMolhoExtra * 1, 'hamComMolho')
                return location.assign('./combos.html')
            }
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + provoloneAd.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2  + 3 + counterMolhoExtra * 1 , 'hamComMolho')
            location.assign('./combos.html')
        }else if(qjReino.checked == true){
            if(provoloneAd.checked == true){
                criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + provoloneAd.value + ', ' + qjReino.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + 7, 'hamComMolho')
                return location.assign('./combos.html')
            }
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + qjReino.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2  + 4 + counterMolhoExtra * 1 , 'hamComMolho')
            location.assign('./combos.html')
        }else{
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds}mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + counterMolhoExtra * 1, 'hamComMolho')
            location.assign('./combos.html')
        }
    }else{
        if(provoloneAd.checked == true){
            if(qjReino.checked == true){
                criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + provoloneAd.value + ', ' + qjReino.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + 7 + counterMolhoExtra * 1, 'hamburguerDaPromo')
                return location.assign('./confirma.html')
            }
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + provoloneAd.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + 3 + counterMolhoExtra * 1, 'hamburguerDaPromo')
            location.assign('./confirma.html')
        }else if(qjReino.checked == true){
            if(provoloneAd.checked == true){
                criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + provoloneAd.value + ', ' + qjReino.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + 7, 'hamburguerDaPromo')
                return location.assign('./confirma.html')
            }
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds + qjReino.value} mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + 4 + counterMolhoExtra * 1, 'hamburguerDaPromo')
            location.assign('./confirma.html')
        }else{
            criaPedidos(getPedidosHam()[0].qtd, getPedidosHam()[0].produto + ` molho ${meuMolho}, adicional de ${adds}mais extra de: ${molhoExtra}`, getPedidosHam()[0].preco + counterAd * 2 + counterMolhoExtra * 1, 'hamburguerDaPromo')
            location.assign('./confirma.html')
        }
    }
    
    
    
})
