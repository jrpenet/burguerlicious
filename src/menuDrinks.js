import {criaPedidosJuices} from './juices'
import {criaPedidos} from './pedidos'

//gera o menu na tabela

const listaDeBebidas = [{
    nomeDaBebida: 'Coca-cola lata (350ml)',
    descricao: '',
    tipo: '',
    valor: 3.5,
    imagem: './images/cocalata.png'
},{
    nomeDaBebida: 'Antártica lata (350ml)',
    descricao: '',
    valor: 3.5,
    imagem: './images/antarticalata.png'
},{
    nomeDaBebida: 'Pepsi 220ml',
    descricao: '',
    valor: 2,
    imagem: './images/pepsi.png'
},{
    nomeDaBebida: 'Guaraná (220ml)',
    descricao: '',
    valor: 2,
    imagem: './images/guarana.png'
},{
    nomeDaBebida: 'Sukita (220ml)',
    descricao: '',
    valor: 2,
    imagem: './images/sukita.png'
},{
   nomeDaBebida: 'Soda (220ml)',
    descricao: '',
    valor: 2,
   imagem: './images/soda.png'
}
//,{
   // nomeDaBebida: 'Coca-cola 1L',
    //descricao: '',
   // valor: 5,
   // imagem: './images/coca1l.png'
//}
,{
    nomeDaBebida: 'Antártica 1L',
    descricao: '',
    valor: 5,
    imagem: './images/antartica1L.png'
},{
    nomeDaBebida: 'Água mineral',
    descricao: '',
    valor: 1,
    imagem: './images/aguasjoana.png'
},{
    nomeDaBebida: 'Água mineral com gás',
    descricao: '',
    valor: 2,
    imagem: './images/aguagas.png'
},{
    nomeDaBebida: 'Suco copo polpa (400ml)',
    descricao: 'Cajá - Cajú - Graviola - Goiaba - Acerola - Manga - Maracujá - Morango - Abacaxi - Abacaxi c/ Hortelã - Pinha e Tamarindo ',
    valor: 3,
    tipo: 'suco',
    imagem: './images/coposuco.png'
},{
    nomeDaBebida: 'Suco jarra (1.200ml)',
    descricao: 'Cajá - Cajú - Graviola - Goiaba - Acerola - Manga - Maracujá - Morango - Abacaxi - Abacaxi c/ Hortelã - Pinha e Tamarindo ',
    valor: 8,
    imagem: './images/jarrasuco.png',
    tipo: 'suco'
}]

//pausados
// ,{
//     nomeDaBebida: 'Caldo de cana',
//     descricao: '',
//     valor: 3,
//     imagem: './images/caldodecana.jpeg'
// },{
//     nomeDaBebida: 'Green Apple',
//     descricao: '',
//     valor: 4,
//     imagem: './images/greenapple.jpeg'
// }

const listaDeBebidasSmoothie = [{
    nomeDaBebida: 'Morango com Banana (400ml)',
    descricao: '',
    valor: 7,
    imagem: './images/smorango.jpeg',
},{
    nomeDaBebida: 'Maracujá, Abacaxi e Laranja (400ml)',
    descricao: '',
    valor: 6,
    imagem: './images/smoothie.jpeg',
}]

const addElementsBebidas = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeDaBebida
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    imagem.setAttribute('src', lanche.imagem)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.valor.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.valor
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeDaBebida
        const price = lanche.valor
        const tipoEl = lanche.tipo

        if(lanche.tipo === 'suco'){
            criaPedidosJuices(qt, prodt, price, tipoEl)
        }else{
            criaPedidos(qt, prodt, price, tipoEl)
        }

        if(lanche.tipo === 'suco'){
            location.assign('./addjuice.html')
        }else{
            location.assign('./txentrega.html')
        }

    })

    return divMain
}

const addElementsBebidasSmoothie = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeDaBebida
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    imagem.setAttribute('src', lanche.imagem)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.valor.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.valor
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeDaBebida
        const price = lanche.valor
        const tipoEl = lanche.tipo
        criaPedidos(qt, prodt, price, tipoEl)
        location.assign('./txentrega.html')
    })

    return divMain
}

const addMenuBebidas = () => {
    listaDeBebidas.forEach((bebida) => {
        const linhaHoriz = document.createElement('hr')
        document.querySelector('#docBebidas').appendChild(linhaHoriz)
        document.querySelector('#docBebidas').appendChild(addElementsBebidas(bebida))
    })
}

const addMenuBebidasSmoothie = () => {
    listaDeBebidasSmoothie.forEach((bebida) => {
        const linhaHoriz = document.createElement('hr')
        document.querySelector('#docSmoothie').appendChild(linhaHoriz)
        document.querySelector('#docSmoothie').appendChild(addElementsBebidasSmoothie(bebida))
    })
}

export {addMenuBebidas, addMenuBebidasSmoothie, addElementsBebidas}