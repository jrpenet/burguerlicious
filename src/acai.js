import {addMenuPromocoes} from './menuPromo'
import {apagaHams} from './hamburguer'
import {apagaRefris, apagaKids} from './refris'
import { criaPedidos } from './pedidos'

apagaHams()
apagaRefris()
apagaKids()

//INFO DOS FORMS
const qtd = document.querySelector('#acaiQty')
//tamanho
const tmnh = document.querySelectorAll('#acaiSize [type=radio]')

tmnh.forEach((tamanho) => {
    tamanho.addEventListener('change', () => {
        if(tamanho.checked == true){
            for(let i=0; i < tmnh.length; i++){
                if(tmnh[i].checked == true){
                    const pai = tmnh[i].parentNode
                    pai.setAttribute('style', 'color: yellow;')
                }

            }
        }
        
        if(tmnh[0].checked == false){
            const pai = tmnh[0].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(tmnh[1].checked == false){
            const pai = tmnh[1].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(tmnh[2].checked == false){
            const pai = tmnh[2].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }
        
    })
})

//cremes
const creme1 = document.querySelector('#acaiCream1')
const creme2 = document.querySelector('#acaiCream2')

//frutas
const fru = document.querySelectorAll('[name=acaiFrt]')

fru.forEach((fruta) => {
    fruta.addEventListener('change', () => {
        if(fruta.checked == true){
            for(let i=0; i < fru.length; i++){
                if(fru[i].checked == true){
                    const pai = fru[i].parentNode
                    pai.setAttribute('style', 'color: yellow;')
                }

            }
        }
        
        if(fru[0].checked == false){
            const pai = fru[0].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(fru[1].checked == false){
            const pai = fru[1].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(fru[2].checked == false){
            const pai = fru[2].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(fru[3].checked == false){
            const pai = fru[3].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(fru[4].checked == false){
            const pai = fru[4].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }
        
    })
})

const fru2 = document.querySelectorAll('[name=morouki]')
const fru3 = document.querySelectorAll('[name=fru3]')

//embutidos
const emb = document.querySelector('#listaAcaiEmbutidos')
const emb2 = document.querySelector('#listaAcaiEmbutidos2')
const emb3 = document.querySelector('#listaAcaiEmbutidos3')

//caldas
const caldas = document.querySelectorAll('.checado8')

caldas.forEach((calda) => {
    calda.addEventListener('change', () => {
        if(calda.checked == true){
            for(let i=0; i < caldas.length; i++){
                if(caldas[i].checked == true){
                    const pai = caldas[i].parentNode
                    pai.setAttribute('style', 'color: yellow;')
                }

            }
        }
        
        if(caldas[0].checked == false){
            const pai = caldas[0].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[1].checked == false){
            const pai = caldas[1].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[2].checked == false){
            const pai = caldas[2].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[3].checked == false){
            const pai = caldas[3].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[4].checked == false){
            const pai = caldas[4].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[5].checked == false){
            const pai = caldas[5].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[6].checked == false){
            const pai = caldas[6].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[7].checked == false){
            const pai = caldas[7].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }

        if(caldas[8].checked == false){
            const pai = caldas[8].parentNode
            pai.setAttribute('style', 'color: red;text-decoration: line-through;')
        }
        
    })
})

const fechaPedido = document.querySelector('.btn-finaliza')
fechaPedido.addEventListener('click', (e) => {
    e.preventDefault()
    console.log('qtd: ' + qtd.value)

    let tamanhoInfo = ''
    
   for(let i =0; i < tmnh.length; i++){
       if(tmnh[i].checked){
           tamanhoInfo = tmnh[i].value
       }
    }

   console.log('tamanho ' + tamanhoInfo)
   
   console.log('creme 1 = '+ creme1.value, 'creme2 = ' + creme2.value)

    let frutaInclusa = ''
    for(let i =0; i < fru.length; i++){
        if(fru[i].checked){
            frutaInclusa = fru[i].value
        }
    }

    console.log('a fruta inclusa é ' + frutaInclusa)

    let rs = ''

    for(let i =0; i < fru2.length; i++){
        if(fru2[i].checked){
            //console.log('fruta2reais ' + fru2[i].value)
            rs += fru2[i].value + ','
        }
    }

    let counterRS = document.querySelectorAll('[name=morouki]:checked').length

    let rs2 = ''

    for(let i =0; i < fru3.length; i++){
        if(fru3[i].checked){
            //console.log('fruta3reais ' + fru3[i].value)
            rs2 += fru3[i].value + ','
        }
    }
    let counterRS2 = document.querySelectorAll('[name=fru3]:checked').length
    console.log('fruta2reais ' + rs)
    console.log('fruta extra 1 real' + rs2)

    console.log('emb1 ' + emb.value)
    console.log('emb2 ' + emb2.value)
    console.log('emb3 ' + emb3.value)

    let infoCaldas = ''

    for(let i =0; i < caldas.length; i++){
        if(caldas[i].checked){
            //console.log('calda ' + caldas[i].value)
            infoCaldas = caldas[i].value
        }
    }
    console.log(infoCaldas)



    //criaPedidos(qtd.value, 'Açaí ')
    if(tamanhoInfo == 'P - 250ml'){
        criaPedidos(qtd.value, 'açaí ' + tamanhoInfo + ` creme de ${creme1.value} + ${creme2.value}, ` + 'fruta escolhida: '+ frutaInclusa + ', adicional de ' + rs + ' ' + rs2 + '. Embutidos: ' + `${emb.value}, ${emb2.value}, ${emb3.value} e calda de ${infoCaldas}`, 10 + (2 * counterRS) + (1 * counterRS2) , 'acai')
        criaPedidos(qtd.value, 'Emb. Açaí', 0.5)
    }else if(tamanhoInfo == 'M - 350ml'){
        criaPedidos(qtd.value, 'açaí ' + tamanhoInfo + ` creme de ${creme1.value} + ${creme2.value}, ` + 'fruta escolhida: '+ frutaInclusa + ', adicional de ' + rs + ' ' + rs2 + '. Embutidos: ' + `${emb.value}, ${emb2.value}, ${emb3.value} e calda de ${infoCaldas}`, 12 + (2 * counterRS)  + (1 * counterRS2) , 'acai')
        criaPedidos(qtd.value, 'Emb. Açaí', 0.5)
    }else{
        criaPedidos(qtd.value, 'açaí ' + tamanhoInfo + ` creme de ${creme1.value} + ${creme2.value}, ` + 'fruta escolhida: '+ frutaInclusa + ', adicional de ' + rs + ' ' + rs2 + '. Embutidos: ' + `${emb.value}, ${emb2.value}, ${emb3.value} e calda de ${infoCaldas}`, 15 + (2 * counterRS)  + (1 * counterRS2) , 'acai')
        criaPedidos(qtd.value, 'Emb. Açaí', 0.5)
    }

    location.assign('confirma.html')
   
})

document.querySelector('.btn-renew').addEventListener('click', (e) => {
    e.preventDefault()

    console.log('qtd: ' + qtd.value)

    let tamanhoInfo = ''
    
   for(let i =0; i < tmnh.length; i++){
       if(tmnh[i].checked){
           tamanhoInfo = tmnh[i].value
       }
    }

   console.log('tamanho ' + tamanhoInfo)
   
   console.log('creme 1 = '+ creme1.value, 'creme2 = ' + creme2.value)

    let frutaInclusa = ''
    for(let i =0; i < fru.length; i++){
        if(fru[i].checked){
            frutaInclusa = fru[i].value
        }
    }

    console.log('a fruta inclusa é ' + frutaInclusa)

    let rs = ''

    for(let i =0; i < fru2.length; i++){
        if(fru2[i].checked){
            //console.log('fruta2reais ' + fru2[i].value)
            rs += fru2[i].value + ','
        }
    }

    let counterRS = document.querySelectorAll('[name=morouki]:checked').length

    let rs2 = ''

    for(let i =0; i < fru3.length; i++){
        if(fru3[i].checked){
            //console.log('fruta3reais ' + fru3[i].value)
            rs2 += fru3[i].value + ','
        }
    }
    let counterRS2 = document.querySelectorAll('[name=fru3]:checked').length
    console.log('fruta2reais ' + rs)
    console.log('fruta extra 1 real' + rs2)

    console.log('emb1 ' + emb.value)
    console.log('emb2 ' + emb2.value)
    console.log('emb3 ' + emb3.value)

    let infoCaldas = ''

    for(let i =0; i < caldas.length; i++){
        if(caldas[i].checked){
            //console.log('calda ' + caldas[i].value)
            infoCaldas = caldas[i].value
        }
    }
    console.log(infoCaldas)



    //criaPedidos(qtd.value, 'Açaí ')
    if(tamanhoInfo == 'P - 250ml'){
        criaPedidos(qtd.value, 'açaí ' + tamanhoInfo + ` creme de ${creme1.value} + ${creme2.value}, ` + 'fruta escolhida: '+ frutaInclusa + ', adicional de ' + rs + ' ' + rs2 + '. Embutidos: ' + `${emb.value}, ${emb2.value}, ${emb3.value} e calda de ${infoCaldas}`, 10 + (2 * counterRS) + (1 * counterRS2) , 'acai')
        criaPedidos(qtd.value, 'Emb. Açaí', 0.5)
    }else if(tamanhoInfo == 'M - 350ml'){
        criaPedidos(qtd.value, 'açaí ' + tamanhoInfo + ` creme de ${creme1.value} + ${creme2.value}, ` + 'fruta escolhida: '+ frutaInclusa + ', adicional de ' + rs + ' ' + rs2 + '. Embutidos: ' + `${emb.value}, ${emb2.value}, ${emb3.value} e calda de ${infoCaldas}`, 12 + (2 * counterRS)  + (1 * counterRS2) , 'acai')
        criaPedidos(qtd.value, 'Emb. Açaí', 0.5)
    }else{
        criaPedidos(qtd.value, 'açaí ' + tamanhoInfo + ` creme de ${creme1.value} + ${creme2.value}, ` + 'fruta escolhida: '+ frutaInclusa + ', adicional de ' + rs + ' ' + rs2 + '. Embutidos: ' + `${emb.value}, ${emb2.value}, ${emb3.value} e calda de ${infoCaldas}`, 15 + (2 * counterRS)  + (1 * counterRS2) , 'acai')
        criaPedidos(qtd.value, 'Emb. Açaí', 0.5)
    }
    
    window.location.reload()
})

document.querySelector('.cart').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./confirma.html')
})

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})