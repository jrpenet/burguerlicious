import {addMenuSobremesas} from './menuSobremesas'
import { apagaBolo } from './pedidosBolo'

addMenuSobremesas()
apagaBolo()

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})