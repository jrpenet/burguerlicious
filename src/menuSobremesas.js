import { criaPedidos} from './pedidos'
import { criaPedidosBolo } from './pedidosBolo'

const cardapioSobremesas = [{
    nomeHamburguer: 'Bolo de Pote',
    descricao: 'Deliciosos bolos de potes, com recheios generosos e de sabor indiscutível. Escolha já o seu sabor preferido.',
    preco: 6,
    tipo: 'sobremesa',
    foto: './images/bolodepoteimg.jpeg'
}]//,{
 //   nomeHamburguer: 'Fatia de torta - Prestígio',
//    descricao: '',
  //  preco: 8,
    //tipo: 'sobremesa',
//    foto: './images/torta.jpeg'
//},{
//    nomeHamburguer: 'Fatia de torta - Diamante Negro',
 //   descricao: '',
  //  preco: 8,
//    tipo: 'sobremesa',
//    foto: './images/torta.jpeg'
//},{
//    nomeHamburguer: 'Fatia de torta - Leite Ninho',
//    descricao: '',
//    preco: 8,
//    tipo: 'sobremesa',
//    foto: './images/torta.jpeg'
//},{
//    nomeHamburguer: 'Cartola',
//    descricao: '',
//    preco: 7,
//    tipo: 'sobremesa',
//    foto: './images/cartola.png'
//},{
//    nomeHamburguer: 'Salada de Frutas',
//    descricao: '',
//    preco: 4,
//    tipo: 'sobremesa',
//    foto: './images/saladaf.png'
//}]

const addElementsSobremesas = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipoEl = lanche.tipo
        if(lanche.nomeHamburguer === 'Bolo de Pote'){
            criaPedidosBolo(qt, prodt, price, tipoEl)
            location.assign('./addsaborbolo.html')
        }else {
            criaPedidos(qt, prodt, price, tipoEl)
            location.assign('./confirma.html')
        }
        
    })

    return divMain
}

const addMenuSobremesas = () => {
    cardapioSobremesas.forEach((bebida) => {
        const linhaHoriz = document.createElement('hr')
        document.querySelector('#docSobremesas').appendChild(linhaHoriz)
        document.querySelector('#docSobremesas').appendChild(addElementsSobremesas(bebida))
    })
}

export {addMenuSobremesas}