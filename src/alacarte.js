
import {apagaHams} from './hamburguer'
import {apagaRefris, apagaKids} from './refris'
import { criaPedidos } from './pedidos'

apagaHams()
apagaRefris()
apagaKids()

//INFO DOS FORMS
const qtd = document.querySelector('#acaiQty')
//tamanho
const tmnh = document.querySelectorAll('#acaiSize [type=radio]')

tmnh.forEach((tamanho) => {
    tamanho.addEventListener('change', () => {
        for(let i=0; i < tmnh.length; i++){
            if(tmnh[i].checked == true){
                const pai = tmnh[i].parentNode
                pai.setAttribute('style', 'color: yellow;')
            }
        }

        for(let i =0; i < tmnh.length; i++){
            if(tmnh[i].checked == false){
                const pai = tmnh[i].parentNode
                pai.setAttribute('style', 'color: red; text-decoration: line-through;')
            }
        }
                
    })
})

//cremes
const creme1 = document.querySelector('#acaiCream1')
const creme2 = document.querySelector('#acaiCream2')


const fechaPedido = document.querySelector('.btn-finaliza')
fechaPedido.addEventListener('click', (e) => {
    e.preventDefault()
    console.log('qtd: ' + qtd.value)

    let tamanhoInfo = ''
    
   for(let i =0; i < tmnh.length; i++){
       if(tmnh[i].checked){
           tamanhoInfo = tmnh[i].value
       }
    }

    criaPedidos(qtd.value, tamanhoInfo + ` com guarnição de ${creme1.value} + ${creme2.value}`, 23, 'prato')
    criaPedidos(qtd.value, 'Emb. à la carte', 1)

    location.assign('confirma.html')
   
})

document.querySelector('.btn-renew').addEventListener('click', (e) => {
    e.preventDefault()

    console.log('qtd: ' + qtd.value)

    let tamanhoInfo = ''
    
   for(let i =0; i < tmnh.length; i++){
       if(tmnh[i].checked){
           tamanhoInfo = tmnh[i].value
       }
    }

    criaPedidos(qtd.value, tamanhoInfo + ` com guarnição de ${creme1.value} + ${creme2.value}`, 23, 'prato')
    criaPedidos(qtd.value, 'Emb. à la carte', 1)
    
    window.location.reload()
})

document.querySelector('.cart').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./confirma.html')
})

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})