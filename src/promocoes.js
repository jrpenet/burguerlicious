import {addMenuPromocoes} from './menuPromo'
import {apagaHams} from './hamburguer'
import {apagaRefris, apagaKids} from './refris'

apagaHams()
apagaRefris()
apagaKids()
addMenuPromocoes()

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})