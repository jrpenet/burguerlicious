let pedidosHam = []

const loadPedidosHam = function(){
    const pedidosHamJSON = sessionStorage.getItem('pedidosHam')
    
    if(pedidosHamJSON !== null){
        return JSON.parse(pedidosHamJSON)
    } else {
        return []
    }
}

const savePedidosHam = function(){
    sessionStorage.setItem('pedidosHam', JSON.stringify(pedidosHam))
}

//expose orders from module
const getPedidosHam = () => pedidosHam

const criaPedidosHam = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosHam.unshift({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        nomeDoBairro: bairro
    })

    savePedidosHam()
}

const removePedidosHam = (item) => {
    pedidosHam.splice(item, 1)
    savePedidosHam()
    window.location.reload()
}

const apagaHams = () => {
    if(getPedidosHam().filter((x) => x.tipo === 'hamburguer')){
        getPedidosHam().splice(0, 1)
        savePedidosHam()        
    }
}

pedidosHam = loadPedidosHam()

export { getPedidosHam, criaPedidosHam, savePedidosHam, removePedidosHam, apagaHams}