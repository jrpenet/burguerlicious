import {criaPedidos} from './pedidos'
import {criaPedidosHam} from './hamburguer'
import { criaPedidosRefris } from './refris'

const cardapioPromocoes = [{
    nomeHamburguer: '2 Trios Fantastic',
    descricao: 'Fantastic + batata frita + refri de 220ml',
    preco: 32,
    tipo2: 'linhakids',
    foto: './images/2trios.jpeg'
},{
    nomeHamburguer: 'Combo Family',
    descricao: '3 Burguerlicious + refri de 1L + batata grande',
    preco: 37,
    tipo: 'hamburguerDaPromo',
    foto: './images/semanamaluca.jpeg'
},//{
   // nomeHamburguer: 'Burguerlicious com batata',
 //   descricao: 'Válido apenas nas Segundas',
   // preco: 10,
    //tipo: 'hamburguerDaPromo',
    //foto: './images/promoseg.jpeg'
//},
//{
//    nomeHamburguer: 'Delicious - Promoção',
  //  descricao: 'Válido apenas nas Terças',
   // preco: 10,
  //  tipo: 'hamburguerDaPromo',
  //  foto: './images/promoter.jpeg'
//},{
    //nomeHamburguer: 'Fantastic - Promoção',
  //  descricao: 'Válido apenas nas Quartas',
   // preco: 10,
   // tipo: 'hamburguerDaPromo',
   // foto: './images/promoqua.jpeg'
//},{
   // nomeHamburguer: 'Chicken - Promoção',
  //  descricao: 'Válido apenas nas Quintas',
  //  preco: 10,
  //  tipo: 'hamburguerDaPromo',
  //  foto: './images/promoqui.jpeg'
//},
{
    nomeHamburguer: 'Combo - Coxinha + refri',
    descricao: '',
    preco: 3,
    tipo: 'freeRefri',
    foto: './images/combocoxinha.jpeg'
},{
    nomeHamburguer: 'Combo - 3 Salgados',
    descricao: '3 deliciosas coxinhas',
    preco: 5,
    tipo: 'promocao',
    foto: './images/combocoxinha.jpeg'
},{
    nomeHamburguer: 'Cachorro Quente - Unidade',
    descricao: '',
    preco: 5,
    tipo: '',
    foto: './images/hotdog.jpeg'
},{
    nomeHamburguer: 'Combo - Cachorro-Quente',
    descricao: '3 cachorros-quentes por R$ 10,00',
    preco: 12,
    tipo: 'comboHotDog',
    foto: './images/hotdog.jpeg'
},{
    nomeHamburguer: 'Combo - Linha Kids',
    descricao: '1 hambúrguer + 2 batatas smile + 1 refri (200ml)',
    preco: 12,
    tipo2: 'linhakids',
    foto: './images/combokids.png'
},{
    nomeHamburguer: '1 Trio Burguerlicious',
    descricao: 'Burguerlicious + batata frita + refri de 220ml',
    preco: 15,
    tipo2: 'linhakids',
    foto: './images/trio1.png'
},{
    nomeHamburguer: '2 Trio Burguerlicious',
    descricao: '2 Burguerlicious + 2 batatas fritas + 2 refris de 220ml',
    preco: 27,
    tipo2: 'linhakids',
    foto: './images/trio2.png'
},{
    nomeHamburguer: 'Super Combo Family',
    descricao: '4 Burguerlicious + refri de 1L + batata grande',
    preco: 45,
    tipo: 'hamburguerDaPromo',
    foto: './images/trio3.png'
}]

const addElementsPromocoes = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer descricaoBebida')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipoEl = lanche.tipo
        const tipoEl2 = lanche.tipo2

        if(lanche.tipo === 'hamburguer'){
            criaPedidosHam(qt, prodt, price, tipoEl)
        }else if(lanche.tipo === 'hamburguerDaPromo'){
            criaPedidosHam(qt, prodt, price, tipoEl)
        }else if(lanche.tipo === 'freeRefri'){
            criaPedidosRefris(qt, prodt, price, tipoEl)
        }else if(lanche.tipo2 === 'linhakids'){
            criaPedidosRefris(qt, prodt, price, tipoEl2)
        }else if (lanche.tipo === 'comboHotDog') {
            criaPedidos(qt, prodt, price, tipoEl)
            criaPedidos(qt, 'Embalagem Hot-Dog', 1, 'Embalagemhotdog')
        }
        else{
            criaPedidos(qt, prodt, price, tipoEl)
        }
        

        if(lanche.tipo === 'freeRefri'){
            location.assign('./addjuice.html')
        }
        else if(lanche.tipo === 'hamburguer'){
            location.assign('./addnsauce.html')
        }else if(lanche.tipo === 'hamburguerDaPromo'){
            location.assign('./addnsauce.html')
        }
        else if(lanche.tipo2 === 'linhakids'){
            location.assign('./addjuice.html')
        }
        else{
            location.assign('./beverage.html')
        }

        
    })

    return divMain
}

const addMenuPromocoes = () => {
    cardapioPromocoes.forEach((lanche) => {
        const linhaHoriz = document.createElement('hr')
        document.querySelector('#docPromocoes').appendChild(linhaHoriz)
        document.querySelector('#docPromocoes').appendChild(addElementsPromocoes(lanche))
    })
}

export {addMenuPromocoes}