let clicks = []

const loadClicks = function(){
    const clicksJSON = sessionStorage.getItem('clicks')
    
    if(clicksJSON !== null){
        return JSON.parse(clicksJSON)
    } else {
        return []
    }
}

const saveClicks = function(){
    sessionStorage.setItem('clicks', JSON.stringify(clicks))
}

//expose orders from module
const getClicks = () => clicks

const criaClicks = (ativo) =>{
    clicks.push(1)
    saveClicks()
}

const removeClicks = () => {
    clicks.splice(0,3)
    saveClicks()
    //window.location.reload()
}

clicks = loadClicks()

export { getClicks, criaClicks, saveClicks, removeClicks}