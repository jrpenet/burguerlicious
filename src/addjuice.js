import {getPedidos} from './pedidos'
import {getPedidosJuices} from './juices'
import {addCardapioSabores, addh1Sabores, addh1RefriCombos, addCardapioRefriCombos} from './menuDiversos'
import { getPedidosRefris } from './refris'

const temSuco = getPedidosJuices().filter((x) => x.tipo === 'suco')
const sucoPedido = getPedidos().filter((x) => x.tipo === 'sucoIn')
const hamMais = getPedidos().filter((x) => x.tipo === 'hamComMolho')


const temRefri = getPedidosRefris().filter((x) => x.tipo === 'freeRefri')
const refriConfirmada = getPedidos().filter((x) => x.tipo === 'refriAdded')
const kids = getPedidosRefris().filter((x) => x.tipo === 'linhakids')

if(temSuco.length > sucoPedido.length || sucoPedido.length <= temSuco.length && getPedidosJuices()[0] != undefined || getPedidosRefris()[0] == undefined){
    const mostraJuice = document.querySelector('.informaPedido')
    mostraJuice.textContent = 'Escolha o sabor do suco'
    const mostraRefri = document.querySelector('#refriCombos')
    mostraRefri.setAttribute('style', 'display: none;')

    addh1Sabores()
    addCardapioSabores()
}


if(getPedidosRefris()[0] == undefined && getPedidosJuices()[0] == undefined){ //|| getPedidosRefris().length === 0 && getPedidosJuices().length
    location.assign('./confirma.html')
}

if(temRefri.length > 0 || kids.length > 0){
    addh1RefriCombos()
    addCardapioRefriCombos()
}

document.querySelector('.btn-avanca').addEventListener('click', (e) => {
    e.preventDefault()
    if(temSuco.length > sucoPedido.length || sucoPedido.length <= temSuco.length){
        alert('Escolha uma opção')
    }else if(temRefri.length > 0 || temRefri.length > refriConfirmada.length){
        alert('Escolha uma opção')
    }else{
        location.assign('./confirma.html')
    }
    
})